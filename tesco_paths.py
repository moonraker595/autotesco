# A place to store all the long paths and URLs
# so not to clog up the code too much
tescoHome = "https://www.tesco.com/groceries/?icid=dchp_groceriesshopgroceries/"
signInBtn = "a.button.button-primary"
submitSignIn = 'button[class="ui-component__button"]'
slotsBtn = '//a[@href="/groceries/en-GB/slots"]'
homeDeliveryBtn = '//a[@href="/groceries/en-GB/slots/delivery"]'
homeDeliveryList = "/html/body/div[1]/div/div[1]/div[2]/div[1]/div/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/ul"
clickAndCollectBtn = '//a[@href="/groceries/en-GB/slots/collection"]'
clickAndCollectList = "/html/body/div[1]/div/div[1]/div[2]/div[1]/div/div[2]/div/div[2]/div/div[2]/div[3]/div[1]/ul"
bottomOfWindow = "window.scrollTo(0, document.body.scrollHeight);"
signOutBtn = "/html/body/div[1]/div/div[1]/div[1]/nav[2]/div/div/div/ul/li[4]/form/button/span/span"
