from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import logging
import time, sys, os
from tesco_paths import *


class AutomateDeliverySlot(object):
    # set up some variables
    def __init__(self, config):
        logging.basicConfig(
            filename="success.log",
            filemode="a",
            format='%(asctime)s %(message)s',
            datefmt="%d/%m/%y at %H:%M:",
            level=logging.INFO,
        )
        self.config = config
        self.name = self.config["name"].upper()
        self.attempt = 0
        self.errors = 0
        self.waitTimeBetweenRuns = 75
        self.startTime = 0
        self.endTime = 0
        self.alreadyWaitedForDelayStart = False

    def initSelenium(self):
        self.driver = webdriver.Chrome()
        self.driver.get(tescoHome)
        self.driver.find_element_by_css_selector(signInBtn).click()

    # main function which kicks everything off
    def start(self):
        print("########################################")
        time = datetime.now().strftime("%H:%M:%S")
        print(f"Name: {self.name}. Start: {time}")
        print(f"Attempt = {self.attempt}. Errors = {self.errors}")
        print("----------------------------------------")
        try:
            self.attempt += 1
            self.checkForDelayedStart()
            self.delayStartOfRun(self.waitTimeBetweenRuns)
            self.startTime = datetime.now()
            self.initSelenium()
            self.signIn()
            self.checkForSlots()
            self.signOut()
            self.shutDown()
        except Exception as e:
            print("!!!!!!!! " + self.name + " APP ERROR !!!!!!!!")
            print(e)
            self.errors += 1
            self.driver.save_screenshot("error_" + str(self.errors) + ".png")
            self.shutDown()
            pass

    # function to wait a desired amount of time so i can set it running
    # early in the morning without waking up!
    def checkForDelayedStart(self):
        if self.config["startTime"] == "now":
            return
        elif (self.config["startTime"] != "now") and (
            self.alreadyWaitedForDelayStart == True
        ):
            return
        else:
            self.delayFirstStart(self.config["startTime"])
            self.alreadyWaitedForDelayStart = True

    # function to delay the start of the first run
    def delayFirstStart(self, startTime):
        y = startTime.split(":")
        x = datetime.today()
        y = x.replace(
            day=x.day + 1, hour=int(y[0]), minute=int(y[1]), second=0, microsecond=0
        )
        delta_t = y - x
        secs = delta_t.seconds + 1
        self.delayStartOfRun(secs)

    # function to delay the start of each run. time is needed
    # between run as tescos limits signins on single IP
    def delayStartOfRun(self, waitFor):
        while waitFor:
            mins, secs = divmod(waitFor, 60)
            tf = "{:02d}:{:02d}".format(mins, secs)
            print("Waiting " + tf, end="\r")
            time.sleep(1)
            waitFor -= 1

    def signIn(self):
        print("INFO: signing in")
        self.driver.implicitly_wait(5)
        username = self.driver.find_element_by_id("username")
        username.clear()
        username.send_keys(self.config["username"])
        time.sleep(1)
        password = self.driver.find_element_by_id("password")
        password.send_keys(self.config["password"])
        selector = submitSignIn
        signin = self.driver.find_element_by_css_selector(selector)
        signin.click()

    def checkForSlots(self):
        print("INFO: scanning for slots")
        slots = self.driver.find_element_by_xpath(slotsBtn)
        slots.click()
        if "home delivery" in self.config["lookFor"]:
            self.homeDelivery()
        if "click and collect" in self.config["lookFor"]:
            self.clickAndCollect()

    def homeDelivery(self):
        print("INFO: checking home delivery")
        homeDelivery = self.driver.find_element_by_xpath(homeDeliveryBtn)
        homeDelivery.click()
        time.sleep(1)
        self.driver.implicitly_wait(5)
        html_list = self.driver.find_element_by_xpath(homeDeliveryList)
        self.checkTabs("HOME", html_list)

    def clickAndCollect(self):
        print("INFO: checking click & collect")
        cAndC = self.driver.find_element_by_xpath(clickAndCollectBtn)
        cAndC.click()
        time.sleep(1)
        self.driver.implicitly_wait(5)
        html_list = self.driver.find_element_by_xpath(clickAndCollectList)
        self.checkTabs("CandC", html_list)

    def checkTabs(self, page, list):
        tabs = list.find_elements_by_tag_name("li")
        self.driver.execute_script(bottomOfWindow)
        for tab in tabs:
            time.sleep(1)
            self.driver.implicitly_wait(5)
            tab.click()
            time.sleep(1)
            bodyText = self.driver.find_element_by_tag_name("body").text
            if "No slots" in bodyText:
                sys.stdout.write("X")
                sys.stdout.flush()
            else:
                logging.info(f'Success for {self.name} with {page} on {tab.text}')
                print("SLOTS FOUND FOR: " + page + " " + tab.text)
                while True:
                    time.sleep(1)
                    print("\a")
        print("\r")

    def signOut(self):
        print("INFO: signing out")
        self.driver.implicitly_wait(5)
        signout = self.driver.find_element_by_xpath(signOutBtn)
        signout.click()

    def shutDown(self):
        self.endTime = datetime.now()
        delta = self.endTime - self.startTime
        print("End, Time Taken: " + str(delta))
        self.driver.close()
