Due to COVID-19, home and collection grocery slots can be hard to come by. This is a simple Selenium app which polls the Tesco website for home deliveries and click and collect slots.

`python start.py`

*  A time to start can be defined in the config file. This is so I can set it to auto start at 6 in the morning without being woken up at midnight when they release the slots. Time needs to be specified in 24 hour, "HH:SS" format. The app will start looking at the next available time that matches. Specifying a particular day to start is not yet supported.
*  It’ll alarm when it **doesn’t** find “No slots” in the page.
*  It looks for any .json in the root dir and uses that to create as a config file.
*  Once it starts alarming, it sits in a while loop so it doesn’t close and kill the chrome instance.
*  It signs out and in again to force reloading of the tabs.
*  You’ll need to add your email and password in the config file.
*  You’ll need selenium `pip install selenium` and pandas `pip install pandas` as well as installing the [chrome driver](https://www.selenium.dev/documentation/en/selenium_installation/installing_webdriver_binaries/)
*  print("\a") is thing that makes the alarm, so you need to make sure your terminal is allowed to print the bell character, this tripped me up when running on macOS.
