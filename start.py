import os, json
from automate_slots import AutomateDeliverySlot
from itertools import cycle
import pandas as pd

# read in the different config files,
# this looks for json files in the current dir
path_to_json = "."
jsonFiles = [
    pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith(".json")
]

# load each of the config file into a class of their own
class_list = []
for singleFile in jsonFiles:
    with open(singleFile) as f:
        class_list.append(AutomateDeliverySlot(json.load(f)))

# run up each class in turn, waiting for each
# one to finish before starting the next
obj_pool = cycle(class_list)
for obj in obj_pool:
    obj.start()
